//this code demonstrates parallel file iteration.
var line_line = require('line-by-line');
filenames = ['baze/tabela01_BEST.dat'];

var arror = [];
var totfiles = 0;
var allfiles = 1;
var result_object = [];
var cand_s = [4,14,15,16,17,18];
var basesize = 1000;
var whole_dtb = false;
params = [1,0,0]
readfiles (params,whole_dtb,basesize,cand_s,filenames, function(clen, fnum){
    //collect results here.

//    console.log(clen)
    totfiles += fnum
    clen.forEach(function(cl){
	result_object.push(cl);
    });
//    result_object.push(clen)

    if (totfiles == allfiles){
	//this means all candidates are ready to go.
	//console.log(result_object);
	console.log(result_object.length+" found!");
	res.send(JSON.stringify(result_object));
	console.log("Ending request!");
	currentJobs --;
	res.end("Ending request");

    }
});


function readfiles (params,whole_database,basesize,cands,filenames,cb){

    filenames.forEach(function (fname){    
	var candidates= [];
	//console.log(cands)
	line_reader = new line_line(fname); // trenutna example baza
	line_reader.on('error', function (err) {
	});	
	line_reader.on('line', function (line) {
	    var line_parts = line.split(" ");
	    //console.log(line);
	    if (cands == null){	   
		if (candidates.length < basesize){
		    if (whole_database == true){		
			candidates.push(line);		
		    }else{		

			var valid = false;
			var null_params = 0;

			//This code part iterates through array of inputted variables, checks if they equal zero and consequentially computes the correct values from the database.

			for (var p = 0; p<params.length; p++){
			    if (params[p] != null && params[p] != ""){
				var index = p+3;
				if (line_parts[index] < params[p]){
				    valid = true;
				}else{
				    valid = false;
				}
			    }
			}
			if (valid == true){
			    candidates.push(line);
			}
		    }
		}
	    }else{

		if (candidates.length < basesize){
		    if (whole_database == true){
			if (cands.indexOf(line_parts[0]) != -1){
			    candidates.push(line);
			}
		    }else{		

			//console.log("cands "+cands+" "+line_parts[0]+" "+line_parts[1]);

			if (cands.indexOf(parseInt(line_parts[0])) != -1 || cands.indexOf(parseInt(line_parts[1])) != -1){

			    var valid = false;
			    var null_params = 0;
			    for (var p = 0; p<params.length; p++){
				if (params[p] != null && params[p] != ""){
				    var index = p+3;
				    if (line_parts[index] < params[p]){
					valid = true;
				    }else{
					valid = false;
				    }
				}
			    }
			    if (valid == true){
				//console.log("cands "+cands+" "+line_parts[0]+" "+line_parts[1]);
				
				candidates.push(line);
			    }
			}
		    }
		}
		
	    }


	});

	line_reader.on('end', function () {
	    console.log("Finished reding the: "+fname)
	    cb(candidates, 1)
	});

    });   
}


