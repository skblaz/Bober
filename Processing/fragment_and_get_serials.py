from sys import argv
import subprocess
import os

def fragment(drug_molecule):
	out_bytes = subprocess.check_output(["./fragment_drug_molecule", "--ligand", drug_molecule, "--receptor", "no.pdb", "--prep", "drug_fragments.pdb", "--seeds", "seeds.txt"], stderr=subprocess.STDOUT, timeout=120)
	

def get_fragment_serials(drug_molecule):
	if os.path.isfile("drug_fragments.pdb"):
		os.remove("drug_fragments.pdb")

	try:
		fragment(drug_molecule)
	except:
		print("Fragmenting failed")
	fragment_serials = set()
	with open("drug_fragments.pdb") as df:
		for line in df:
			if line.startswith("REMARK   8 RIGID"):
				fragment_serial = (line.split()[3]).strip()
				fragment_serials.add(fragment_serial)
	return fragment_serials
	

def main( ):
	print(sorted(get_fragment_serials(argv[1]), reverse=True))

	
if __name__ == "__main__":
	main( )
		
