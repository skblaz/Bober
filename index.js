
//this is the main server funciton> it serves to call processing functions, which consequentially generate the database itself!

var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var line_line = require('line-by-line');
var fs = require('fs');
var router = express.Router();
var path = __dirname + '/views/'; // Under views there is website data.
var http = require('http');

currentJobs = 0

app.use(bodyParser.urlencoded({ extended: true }));
// app.use('/', express.static(__dirname + '/www'));
app.use(express.static(__dirname + '/public'));
//app.use('/', express.static(__dirname + '/public'));
// handle requests
app.post('/get_data_bober', function (req, res) {

    //mogoce bo treba dat 1 za parameter.
    if (currentJobs < 150){
	currentJobs ++;
	console.log("Currently, there are "+currentJobs+" running.");
	res.setHeader('Content-Type', 'application/json');
	
	//lista 3h parametrov.. to se dodeluje, ostale zanke se same prilagodijo! To se dodela po zelji.
		
	var max_arguments = 3;
	var cand_idates = req.param('cand_s');

	//	var cores_option = req.param('core_check');

	// if (cores_option[1] == 'true'){
	//     cand_idates = (function(cands){
		
	// 	return cands
	//     })(cand_idates)	    
	// }
	
	//console.log(cores_option);
	console.log("Ready to process:"+ cand_idates.length+" candidates..");
	
	//console.log(cand_idates);
	//tukej bo pogledal, ali je candidate dejansko na prvem ali nekem mestu, ce je potem nadaljuje s izvedbo!

	var params = [req.param('cri_1'),req.param('cri_2'),req.param('cri_3')];
	var totfiles = 0;
	var whole_database = false;
	var null_params = 0;
	var basesize = 1000;
	
	params.forEach(function(param){
	    if (param == ''){null_params++;}
	});
	
	console.log(params);
	if (null_params == max_arguments){whole_database = true; console.log("Whole database")};
	console.log("Processing data with> "+(max_arguments-null_params)+" criteria.");	
	var result_object = []; // tukej bo dal rezultate!
	var filenames = [];
	
	for (var k = 0;k<7;k++){
	    var peh =  k<10 ? "0"+k : k
	    filenames.push("baze/"+ req.param('family')+"/BASE"+peh);
	}
	console.log(filenames);
	var allfiles = filenames.length;

	//baze/tabela01_BEST.dat // files, from which it will be read. POSSIBLE MULTIPLE ENTRIES!
	// cand_idates = (function(can){
	//     var tmp = [];
	//     can.forEach(function(idate){
	// 	tmp.push(idate.split("_")[1]);
	//     });		
	// })(cand_idates);
	// console.log(cand_idates);

	readfiles (params,whole_database,basesize,cand_idates,filenames, function(clen, fnum){

	    totfiles += fnum
	    clen.forEach(function(cl){
		result_object.push(cl);
	    });

	    if (totfiles == allfiles){
		console.log(result_object.length+" found!");
		res.send(JSON.stringify(result_object));
		console.log("Ending request!");
		currentJobs --;
		res.end("Ending request");
	    }
	});


    }else{
	console.log("Currently, there are"+currentJobs+" running. This is too many jobs, please come back later..");
	res.send(JSON.stringify({ busy: 1 }));
    }

});

app.post('/get_cand_drug', function (req, resL) {

    var smiles =  '"'+req.param('smiles_drug')+'"';
    console.log("Processing: "+smiles);
    var id = require('crypto').randomBytes(10).toString('hex');
    //var command = 'cd Processing; ./obabel '+' -i smi -:'+smiles+' -omol2 --gen3D > tmp/'+smiles+'.mol2; python3 fragment_and_get_serials.py tmp/'+smiles+'.mol2';
    var command = 'cd Processing; obabel '+' -i smi -:'+smiles+' -omol2 --gen3D > tmp/'+id+'.mol2; python3 fragment_and_get_serials.py tmp/'+id+'.mol2';
    console.log(command);
    var child = require('child_process').exec(command,function(err,stdout,sterr){

	console.log(stdout);
	resL.send(stdout);
	resL.end();

    });

});


app.post('/get_cand_draw', function (req, resD) {

    var smiles =  '"'+req.param('smiles_draw')+'"';
    console.log("Processing: "+smiles);

    //komanda:  all_cores.fs -s Fc1ccccc1 -osmi -xt
    var command = 'cd Processing; obabel '+'all_cores.fs -s '+smiles.replace('"',"'").replace('"',"'")+' -osmi -xt';
    console.log(command);
    var child = require('child_process').exec(command,function(err,stdout,sterr){

    	//console.log(stdout);
    	resD.send(stdout);
    	resD.end();

    });

});


app.post('/get_candidates', function (req, resC) {
    var filter = [req.param('cri_all_atoms'),
		  req.param('cri_core_atoms'),
		  req.param('cri_join_atoms'),
		  req.param('cri_acceptor_atoms'),
		  req.param('cri_donor_atoms'),
		  req.param('cri_cyclic_atoms'),
		  req.param('cri_aromatic_atoms')
		 ];
    
    var ocheck = req.param('oxy_check');
    var ncheck = req.param('nitro_check');
    var scheck = req.param('nitro_check');
    //var cores_option = req.param('core_check');

    //core dobi, potem pa to uporabi naprej
    //var family = req.param('family');

    console.log(ocheck+ncheck+scheck);
    var basePATH = "baze/parameters.dat";
    var pushrow = false;
    line_reader = new line_line(basePATH); // trenutna example baza
    var body = [];
    var outjson = new Object();
    line_reader.on('line', function (line) {
	//console.log(line);
	
	var entries = line.split("|");

	var criteria = entries[1].split(" ").slice(1,1+7);
	var presence = entries[2];
	//console.log(presence);
	var id = entries[0];
	var triggerAppend = 0;
	//number of criteria here
	if (filter.length == 7){
	    for (var j =0; j < 7;j++){
		//	    console.log(criteria[j]+" "+filter[j].split("-")[1]+" "+filter[j].split("-")[0]);

		if(parseInt(criteria[j]) <= filter[j].split("-")[1] && parseInt(criteria[j]) >= filter[j].split("-")[0]){
		    //console.log(criteria[j]+"-----"+filter[j]);


		    triggerAppend++;
		}
	    }
	}
	// if (triggerAppend == 7){
	//     console.log("Finnito..................."+triggerAppend);
	// }

	if (ocheck == "true" && ~presence.indexOf("O") == 1){
	    
	    pushrow = true;

	}else if(ncheck == "true" &&  ~presence.indexOf("N") == 1){
	    
	    pushrow = true;

	}else if(scheck == "true" &&  ~presence.indexOf("S") == 1){
	    
	    pushrow = true;

	}else if (ncheck == "false" && ocheck == "false" && scheck == "false"){

	    pushrow = true;
	    
	}else{
	    
	    pushrow = false;
	    
	}
	
	//this can grow!
	if (triggerAppend == 7 && pushrow == true){
	    
	    body.push(id.replace(/ /g,""));

	}
	
	triggerAppend = 0;
    });


    line_reader.on('end', function () {

	// tukej bo uposteval core atom stvari
	
	//var cores = JSON.parse(fs.readFileSync('baze/coreindex/cores.json', 'utf8'));
	
	// execute a function, which takes as arguments desired core option, body and of course returns new body.
	
	outjson.candidates = body;
	console.log("Found: "+body.length+" candidates");
	resC.send(JSON.stringify(outjson));
	resC.end()
	
    });

    line_reader.on('error', function (err) {
	console.log('error reding file..');
	//resC.end();
    });

});

//This is temporary function, which enables download of whole datadump.
app.get('/download_bober', function (req, res) {
    res.attachment('baze/tabela01_BEST.dat');
    require('http').get('http://localhost:4000/', function(response) {
	response.pipe(res);
    }).on('error', function(err) {
	res.send(500, err.message);
    });
});

app.get('/get_cores', function (req, res) {

    console.log("Sending core json.")
    res.send(fs.readFileSync('baze/coreindex/cores.json', 'utf8'))
    res.end()

});


app.get('/API/:id', function(req, res) {
    //this gets wanted criteria and then displays the whole database according to the provided critkeria. This search will include a similar algorithm to main database search, the main problem will be encoding the data in a way it actually works.

    //the filtered data comes here..
    // res.send(req.params.id+" I was on the server");
    var apistring = req.params.id.split("_");
    
    // candidates = [1,2,3], params = [1,2,3], family= str
    //res.send(apistring[1]+" I was on the server");
    var apistring = "401,402_1.5,null,null_both";
    apistring = apistring.split("_");
    //console.log(apistring[0],apistring[1],apistring[2]);
    var max_arguments = 3;
    var cand_idates = apistring[0].split(","); // []

    console.log(cand_idates);
    console.log("Ready to process:"+ cand_idates.length+" candidates..");
    //console.log(cand_idates);
    //tukej bo pogledal, ali je candidate dejansko na prvem ali nekem mestu, ce je potem nadaljuje s izvedbo!

    var params = apistring[1].split(",");
    var totfiles = 0;
    var whole_database = false;
    var null_params = 0;
    var basesize = 1000;
    
    params.forEach(function(param){

 	if (param == "null"){	    
	    null_params++;
	}else{
	    param = parseInt(param);
	}
    });
    
    console.log(params)
    if (null_params == max_arguments){whole_database = true; console.log("Whole database")};
    console.log("Processing data with> "+(max_arguments-null_params)+" criteria.");	
    var result_object = []; // tukej bo dal rezultate!
    var filenames = [];

    
    for (var k = 0;k<7;k++){
 	var peh =  k<10 ? "0"+k : k
 	filenames.push("baze/"+ apistring[2]+"/BASE"+peh); //both,norel,related
    }
    console.log(filenames);
    var allfiles = filenames.length;
    //baze/tabela01_BEST.dat // files, from which it will be read. POSSIBLE MULTIPLE ENTRIES!
    readfiles (params,whole_database,basesize,cand_idates,filenames, function(clen, fnum){

 	totfiles += fnum
 	clen.forEach(function(cl){
 	    result_object.push(cl);
 	});

 	if (totfiles == allfiles){
 	    console.log(result_object.length+" found!");
 	    res.send(JSON.stringify(result_object));
 	    console.log("Ending request!");
 	    currentJobs --;
 	    res.end("Ending request");
 	}
    });
    // ta del morm se dodelati!

    
});

app.on('uncaughtException', function (exception) {
    console.log("Reading files not complete, this entry doesn't have sufficient data.");
});

router.use(function (req,res,next) {
    console.log("/" + req.method);
    next();
});
// router.get("/",function(req,res){
//     res.sendFile(path + "index.html");
// });
app.use("/",router);

app.listen(4000, function (err) {    
    if (err) {
	throw err
    }
    
    console.log('Server started on port 4000')
})

function exists(el, array){

    var exist = false;
    for (var k =0; k< array.length;k++){
	if(array[k] == el){
	    exists=true;
	}
    }
    return exist   
} 

// function executeOutfile(procname, callback){
//     var child = require('child_process').exec('python celulas.py')
//     child.stdout.pipe(process.stdout)
//     child.on('exit', function() {

// 	process.exit()

// 	callback();
//     })
// }
function readfiles (params,whole_database,basesize,cands,filenames,cb){
    //console.log("Function called");

    cands = cands.map(Number);
    filenames.forEach(function (fname){    
	var candidates= [];
	//console.log(cands)
	line_reader = new line_line(fname); // trenutna example baza
	line_reader.on('error', function (err) {
	    console.log("Failed to read the file..");
	    cb([], 1);
	});	
	line_reader.on('line', function (line) {
	    var line_parts = line.split(" ");
	    //console.log(line);
	    if (cands == null){	   
		if (candidates.length < basesize){
		    if (whole_database == true){		
			candidates.push(line);		
		    }else{		

			var valid = false;
			var null_params = 0;

			//This code part iterates through array of inputted variables, checks if they equal zero and consequentially computes the correct values from the database.

			for (var p = 0; p<params.length; p++){
			    if (params[p] != null && params[p] != ""){
				var index = p+3;
				if (line_parts[index] < params[p]){
				    valid = true;
				}else{
				    valid = false;
				}
			    }
			}
			if (valid == true){
			    candidates.push(line);
			}
		    }
		}
	    }else{

		if (candidates.length < basesize){
		    if (whole_database == true){
			if (cands.indexOf(line_parts[0]) != -1){
			    candidates.push(line);
			}
		    }else{		

			//console.log("cands "+cands+" "+line_parts[0]+" "+line_parts[1]);

			if (cands.indexOf(parseInt(line_parts[0])) != -1 || cands.indexOf(parseInt(line_parts[1])) != -1){

			    
			    
			    var valid = false;
			    var null_params = 0;
			    for (var p = 0; p<params.length; p++){
				if (params[p] != null && params[p] != ""){
				    var index = p+3;
				    if (line_parts[index] < params[p]){
					valid = true;
				    }else{
					valid = false;
				    }
				}
			    }
			    if (valid == true){
				
				candidates.push(line);
			    }
			}
		    }
		}
		
	    }


	});

	line_reader.on('end', function () {
	    console.log("Finished reding the: "+fname)
	    cb(candidates, 1)
	});

    });   
}
