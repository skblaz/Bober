// this is main site script, this is used to process and query the server
// first recieve boundary conditions and send the in the form of param 1,2,3 3...n
// database should do other stuff.

var general_url = "localhost:4000";
url_main = "http://"+general_url+"/get_data_bober";
url_step1 = "http://"+general_url+"/get_candidates";
url_cores = "http://"+general_url+"/get_cores";
url_smile_drug = "http://"+general_url+"/get_cand_drug";
url_smile_draw = "http://"+general_url+"/get_cand_draw";
all_candidates = [];
global_candidates = [];
all_toggled = false;
global_all = []; 
selection_histroty = [];

//main alert html, including progress bar
var alerthtml = "<div class='alert alert-success' "
    +"role='alert'><strong> many candidates being generated!.. </strong> "
    +"Please wait a moment.</div><br><br><center><img src='slike/loadB3.gif' "
    +"alt='Lscrean' style='width:100px;height:70px;'></center></div><br><br>"
    +"<div class='progress'><div class='progress-bar "
    +"progress-bar-success progress-bar-striped active progress1' "
    +"role='progressbar' style='width:0%'><div id='cnumber'></div></div>"
    +"<div class='progress-bar progress-bar-danger progress-bar-striped active progress2' "
    +"role='progressbar' style='width:0%'><div id='cnumberf'></div></div></div>";

// initiate events on load.
$(document).ready(function(event){

    $('#result_options').hide();
    $('[data-toggle="instructions_popover"]').popover({ trigger: "hover" });
    $('#options').hide();
    $('#main_results').hide();
    activate_subtab('parameter_search','draw_structure','draw_drug');

    $.ajax({
	
        url: url_cores,
	
        type: 'GET',
	
        success: function (data) {

	    globalcores = JSON.parse(data)

	    // global_evade = (function(cores){
		
	    // 	var out = []
	    // 	for (var k in cores){
	    // 	    cores[k].forEach(function(x){
	    // 		out.push(x)
	    // 	    })
	    // 	}
		
	    // 	return out
		
	    // })(globalcores)


	    global_evade_list = (function(cores){

		var out = []
		
	    	for (var k in cores){

	    	    out.push(cores[k])
	    	    
	    	}
		console.log("Evade list constructed..");
		
		return out		
	    })(globalcores);
	    

	},
	error: function (xhr, status, error) {
	    console.log("Cores not found..")
	}
    });

});

// Prototype for checking files, not currently used
function CheckFile(name,cb){
    $.get(name)
	.done(function() {	    
	    cb(true);
	}).fail(function() { 
	})
}

// this IS currently used
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

// first part of the app
$("#submit_query_candidates").click(function(e){


    // prepare inputs
    e.preventDefault();
    $('#load_screen').modal('show');
    $("#custom_alert").html("<div class='alert alert-success' role='alert'><strong> Generating candidates.. </strong> Please wait a moment.</div><br><br><center><img src='slike/pie.svg' alt='Lscrean' style='width:100px;height:70px;'></center>");


    // the ajax call
    $.ajax({
	
        url: url_step1,
	
        data: {
	    
	    'cri_all_atoms': $('#all_atoms').val(),
	    'cri_core_atoms': $('#core_atoms').val(),
	    'cri_join_atoms': $('#join_atoms').val(),
	    'cri_acceptor_atoms': $('#acceptor_atoms').val(),
	    'cri_donor_atoms':$('#donor_atoms').val(),
	    'cri_cyclic_atoms': $('#cyclic_atoms').val(),
	    'cri_aromatic_atoms': $('#aromatic_atoms').val(),
	    'oxy_check': $('#oxy_check').prop('checked'),
	    'nitro_check': $('#nitro_check').prop('checked'),
	    'sulfur_check': $('#sulf_check').prop('checked')
	   // 'core_check' : [$('#enrich_core').prop('checked'),$('#exclude_core').prop('checked')]
	  //  'family': family_send,

	},
	
        type: 'POST',
	
        success: function (data1) {

	    //update alert
	    $.when( $("#custom_alert").html(alerthtml)).done(function () {

		//parse and construct the table
	    var json = JSON.parse(data1);
		    
	    if (json.candidates.length > 600){
		$("#custom_alert").html(alerthtml);
	    }
	    var html_step1 =
		"<p class='h1'>Current ligand selection</p>"
		+"<table id='data_table_step1' class='table table-striped' cellspacing='0'>"
		+"<input id='t_cands' type='button' class='button btn-success' value='Toggle selection' onclick='toggle_candidates()'></input>"
		+"<thead>"
		+"<tr>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"<th></th>"
		+"</tr>"
		+"</thead>"
		+"<tbody>";
		//counter initiation
		html_step1 += "<tr class='tableformat'>";
		var counter = 0;
		var counterfp = 0;
		var fpcount = 0;
		all_candidates = json.candidates;
		json.candidates.forEach(function(candidate){
		    
		    var url = 'lepse_slikice/'+candidate+".png";

		    if (UrlExists(url) == true){
			    //update progress
		    counter ++;
		    if (counter % 5 == 0){

			$('#cnumber').html("<p>"+Math.round(counter*100/all_candidates.length)+"% valid candidates loaded.</p>");
			$('.progress1').css('aria-valuenow',Math.round(counter*100/all_candidates.length));
			$('.progress1').css('width',Math.round(counter*100/all_candidates.length)+"%");
		    }
		    global_all.push(candidate);
		    
		    if (counter %9 == 0){
			var candID = "cand_"+candidate.replace(/\D/g,'');
			html_step1 += "<td><img id='"+candID+"' class='lig_image_s2' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
			html_step1 += "</tr>";
			html_step1 += "<tr>";
		    }else{
			var candID = "cand_"+candidate.replace(/\D/g,'');
			html_step1 += "<td><img id='"+candID+"' class='lig_image_s1' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
		    }
		    
		    }else{
		        // update fp progress
		        counterfp++;
		    
		        $('#cnumberf').html("<p>"+Math.round(counterfp*100/all_candidates.length)+"% Invalid candidates.</p>");
		        $('.progress2').css('aria-valuenow',Math.round(counterfp*100/all_candidates.length));
		        $('.progress2').css('width',Math.round(counterfp*100/all_candidates.length)+"%");
		    
		    }
		    		    
		});
		//construct the table..
		html_step1 += "</tr>";
		html_step1 += "</tbody></table>";
	    var html_step1_1 = "<br><div class='form-group'><label class='col-md-5 control-label' for='button2id'>Add candidates and proceed..</label><div class='col-md-6'><input type='button' id='submit_cds' class='btn btn-success' value='Submit criteria'></div></div><br><br>";		

	    $('#dat_table_step1').html(html_step1);
	    $('#dat_proceed_step1').html(html_step1_1);
	    $("#submit_cds").click(function(e){
		e.preventDefault();

		$("#step2").css('display','block');

		$("html,body").animate({ scrollTop: 800 }, "slow");

	    });	    

	    $('#load_screen').modal('hide');  //display loading dialog.
	    $('#imagesearch').css('display','block');
	    var top_step1 = $('#dat_table_step1').position().top;
	    $("html,body").animate({ scrollTop: 900 }, "slow");

	    $('#example').DataTable();

	    });
	},
	error: function (xhr, status, error) {

	    $("#custom_alert").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> Request couldn't be sent. Please try again.</div>");
	}
    });
});




// this is second part of the application
$("#submit_query").click(function(e){
    e.preventDefault();
    
    $('#load_screen').modal('show');  //display loading dialog.
    $("#custom_alert").html("<div class='alert alert-success' role='alert'><strong> Checking the input.. </strong> Please wait a moment.</div><br><br><center><img src='slike/loadB.gif' alt='Lscrean' style='width:100px;height:70px;'></center>");
    var input_ok = checked_input();
    
    if (input_ok == true){
	//different alert here, temporarily
	$("#custom_alert").html("<div class='alert alert-success' role='alert'><strong> Querying the databse.. </strong> Please wait a moment.</div><br><br><center><img src='slike/loadB.gif' alt='Lscrean' style='width:100px;height:70px;'></center>");
	// to se spremeni, ko dejansko objavimo na kako stran!


	// Obtain all three values with query inputs..
	var criterion_1 = $("#value_1").val()
	var criterion_2 = $("#value_2").val()
	var criterion_3 = $("#value_3").val()
	// obtain structure criteria..

	$('input[type="checkbox"]').each(function(){
            var v = $(this).attr('checked') == 'checked'?1:0;
	    console.log(v+" "+$(this).attr('id'));
	    // to gre verjetno v hashmap ki gre potem v data.
	});
	
	if(global_candidates.length > 300){
	    global_candidates = global_candidates.slice(0,300);
	}
	
	//	console.log(globalcores)
	if ($('#enrich_core').prop('checked') == true){	    

	    global_candidates = (function(cands,cores){
		
		//go by each row.
		var finalcands = []
		for (var k in cores){
		    var addition = []
		    addition.push(k)
		    cores[k].forEach(function(subcan){
			addition.push(subcan)
		    })
		    
		    //console.log(addition)
		    cands.forEach(function(ac){
			if (addition.indexOf(ac.match(/\d+/)[0]) != -1){
			    addition.forEach(function(ac2){
				if (finalcands.indexOf(ac2) == -1){
				    finalcands.push(ac2)
				}
			    })
			}
		    })
		}

		if (finalcands.length > 1000){
		    finalcands = finalcands.slice(1,999)}	
		
		return finalcands

	    })(global_candidates,globalcores)

	}

	$.ajax({
	    
            url: url_main,
	    
            data: {
		'cri_1': criterion_1,
		'cri_2': criterion_2,
		'cri_3': criterion_3,
		'cand_s': global_candidates,
		//'core_check' : [$('#enrich_core').prop('checked'),$('#exclude_core').prop('checked')],
		'family': (function(){
		    var tosend;
		    $("input[name='optradio3']").each(function(){
			if($(this).is(':checked')){		    
			    tosend =  $(this).attr('id');
			    console.log("This is radio!:"+ tosend);
			}
		    });
		    
		    return tosend
		    
		})(),
	    },
	    
            type: 'POST',
	    
            success: function (data) {

		//construct the table
		$.when( $("#custom_alert").html(alerthtml)).done(function () {

		var html_table =
		    "<table id='data_table' class='table table-striped' cellspacing='0'>"
		    +"<thead>"
		    +"<tr>"
		    +"<th> overall HD  <span class='glyphicon glyphicon glyphicon-sort-by-attributes-alt' aria-hidden='true'></span></th>"
		    +"<th> core_HD <span class='glyphicon glyphicon glyphicon-sort-by-attributes-alt' aria-hidden='true'></span></th></th>"
		    +"<th> HD of H_bonding atoms <span class='glyphicon glyphicon glyphicon-sort-by-attributes-alt' aria-hidden='true'></span></th></th>"
		    +"<th>From</th>"
		    +"<th>Fo</th>"
			+"</tr>"
			+"</thead>"
			+"<tbody>";
		    // pairs could be saved for future use!
		    var ucount = 0
		    var counter = 0;
		    var counterfp = 0;

		    for (var l = 0;l<data.length;l++){ //data.length
			splitted_data = data[l].split(" ");
			var slika1 = 'dosti_slikic_po_parih/'+splitted_data[8]+"_with_"+splitted_data[9]+".jpg";
			var slika2 = 'dosti_slikic_po_parih/'+splitted_data[9]+"_with_"+splitted_data[8]+".jpg";
	
			// check files, current threshold is 1000 results.
			if (counter < 1000){
			    
			    if (UrlExists(slika1) == true && UrlExists(slika2) == true){
			    
				counter ++;

				if (counter % 5 == 0){
				    //console.log(Math.round(counter*100/all_candidates.length)+"% "+counter+" "+all_candidates.length);
				    $('#cnumber').html("<p>"+Math.round(counter*100/all_candidates.length)+"% valid candidates loaded.</p>");
				    $('.progress1').css('aria-valuenow',Math.round(counter*100/all_candidates.length));
				    $('.progress1').css('width',Math.round(counter*100/all_candidates.length)+"%");
				}


				//globalcores - use this to test for evasion

				// for (key in globalcores){
				//     if (globalcores[key].indexOf(splitted_data[8]) != -1 || globalcores[key].indexOf(splitted_data[9]) != -1){
				// 	html_table += "<tr id='core_evade'>";
				//     }else{
				// 	html_table += "<tr>";
				//     }					
				// }
				
				var evadeTrigger = false;
				
				global_evade_list.forEach(function(sublist){
				    if (sublist.indexOf(splitted_data[8]) != -1 && sublist.indexOf(splitted_data[9]) != -1){
					evadeTrigger = true					
				    }
				})
				
				if (evadeTrigger){
				    html_table += "<tr id='core_evade'>";
				}else{
				    html_table += "<tr>";
				}

				for (var k = 0; k<splitted_data.length;k++){
			
				    if (k == 3){
					
					html_table += "<td>"+parseFloat(splitted_data[k]).toFixed(3)+"</td>";
					
				    }else if (k == 4){
					
					html_table += "<td>"+parseFloat(splitted_data[k]).toFixed(3)+"</td>";
					
				    }else if (k == 5){
					
					html_table += "<td>"+parseFloat(splitted_data[k]).toFixed(3)+"</td>";
					
				    }else if (k == 8){
					
					html_table += "<td><img class='lig_image' src='"+slika1+"' style='width:100px;height:100px;'></td>";
														
				    }else if(k == 9){
				
					html_table += "<td><img class='lig_image' src='"+slika2+"' style='width:100px;height:100px;'></td>";
					
				    }

				    else{}			
				}
							

			    }else{

			    	counterfp++;
				
			    	$('#cnumberf').html("<p>"+Math.round(counterfp*100/all_candidates.length)+"% Invalid candidates.</p>");
			    	$('.progress2').css('aria-valuenow',Math.round(counterfp*100/all_candidates.length));
			    	$('.progress2').css('width',Math.round(counterfp*100/all_candidates.length)+"%");
				
			    }
			}
		    }

		    html_table += "</tbody></table>";
		    $('#no_results').hide();
		    $('#main_dataset').html(html_table);
		    
		    //$('#data_table').DataTable();
		    resulttable = $('#data_table').DataTable();
		    
		    $('#load_screen').modal('hide');  //display loading dialog.
		    window.scrollTo(0,0);
		    $('.nav-tabs a[href="#main_results"]').tab('show');
		    $('.nav-tabs a[href="#search"]').tab('show');
		    activate_tab('results_content','main_content');
		    $("#main_search").hide();
		    $('#result_options').show();
		}); /// tole je loading thing..
		
	    },
	    error: function (xhr, status, error) {

	    	$("#custom_alert").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> Request couldn't be sent. Please try again.</div>");
	    }
	});

    }else{

	$("#custom_alert").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No results..</div>");
	
    }
});


// reset
$("#reset_query").click(function(e){
    e.preventDefault();
    console.log("This will clean all entries..");
});

// canvas operations
function restore_canvas(cbtn){
    var which = $(cbtn).attr('name');
    if (which == 1){
	jsmeApplet.reset();
    }else{
	jsmeApplet2.reset();
    }
}
//prototype
var criterion = function(input_line){
    
    return input_line
};
//prototype
var checked_input = function(){
    
    console.log("Input check funciton started!");
    return true;

}

//activate, avoid iteration as it is overhead in this case
function activate_tab(tab,tab2){
    
    $('#'+tab).show();
    
    $('#'+tab2).hide();
}; 			

function activate_subtab(tab,tab2,tab3){

    
    $('#'+tab).show();
    
    $('#'+tab2).hide();

    $('#'+tab3).hide();
};

// toggle button candidates
function apc(img_obj){

    var candidate = $(img_obj).attr("id");

    if (global_candidates.includes(candidate)==true){
	$("#"+candidate).css('border','1px solid black');
	var index = global_candidates.indexOf(candidate); 
	global_candidates.splice(index, 1);
	//global_candidates.push(candidate);
    }else{	
	$("#"+candidate).css('border','2px dotted orange');
	global_candidates.push(candidate);
    }
    //console.log(global_candidates);

}

//no need for map function as it just reassigns the list.

function toggle_candidates(){

    if (all_toggled == true){
	all_candidates.forEach(function(can){
	    $("#cand_"+can).css('border','1px solid black');
	    all_toggled=false;
	});
	global_candidates = [];
    }else{
	all_candidates.forEach(function(can){
	    $("#cand_"+can).css('border','2px dotted orange');
	    all_toggled=true;
	});
	global_candidates = all_candidates;
    }    
    //this should grap global list, find by id all candidates, change their css from pressed to default or obratno, then add or subtract values from step_1_working list.
}

// list inclusion
function includes(k) {
    for(var i=0; i < this.length; i++){
	if( this[i] === k || ( this[i] !== this[i] && k !== k ) ){
	    return true;
	}
    }
    return false; 
}

// range generation, this is beta
function generateRange(){
    var smiles = $('#smiles_input').val();

    var all_count = 0;
    smiles.split("").forEach(function(el){
	all_count ++;
    })

    $('#all_atoms').val(parseInt(all_count*0.7)+"-"+parseInt(all_count*1.3));
    
}


// smiles drawing
function getSmiles_drug() {

    $('#load_screen').modal('show');
    $("#custom_alert").html("<div class='alert alert-success' role='alert'><strong> Generating candidates.. </strong> Please wait a moment.</div><br><br><center><img src='slike/pie.svg' alt='Lscrean' style='width:100px;height:70px;'></center>");

    $.ajax({

	url : url_smile_drug,
	type : 'POST',
	data : {
            'smiles_drug' : jsmeApplet2.smiles()
	},

	success : function(data) {              
            //console.log(data);
	    var candidates = data.replace("[","").replace("]","").replace(/'/g,"").split(",");
	    candidates = $.map( candidates, function( a ) {
		return a.replace(" ","");
	    });
	    if (data.indexOf("Fragmenting") == -1 || candidates != []){
		$('#load_screen').modal('hide');

	    	var html_step1 =
		    "<p class='h1'>Current ligand selection</p>"
		    +"<table id='data_table_step1' class='table table-striped' cellspacing='0'>"
		    +"<input id='t_cands' type='button' class='button btn-success' value='Toggle selection' onclick='toggle_candidates()'></input>"
		    +"<thead>"
		    +"<tr>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"</tr>"
		    +"</thead>"
		    +"<tbody>";
		
		html_step1 += "<tr>";
		var counter = 0;

		if (candidates.length > 0){

		    //global_all = [];
		    all_candidates = [];
		    //similar steps to the ones before.
		    candidates.forEach(function(candidate){

			var url = 'lepse_slikice/'+candidate+".png";

			if (UrlExists(url) == true){
			    console.log(candidate);
			    counter ++;
			    global_all.push(candidate);
			    all_candidates.push(candidate);
			    if (counter %9 == 0){

				var candID = "cand_"+candidate.replace(/\D/g,'');
				console.log(candID);
				html_step1 += "<td><img id='"+candID+"' class='lig_image_s2' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
				html_step1 += "</tr>";
				html_step1 += "<tr>";
			    }else{
				var candID = "cand_"+candidate.replace(/\D/g,'');

				html_step1 += "<td><img id='"+candID+"' class='lig_image_s1' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
			    }
		

			}
			
		
		    });

		    html_step1 += "</tr>";
		    html_step1 += "</tbody></table>";
		    var html_step1_1 = "<br><div class='form-group'><label class='col-md-5 control-label' for='button2id'>Add candidates and proceed..</label><div class='col-md-6'><input type='button' id='submit_cds' class='btn btn-success' value='Submit criteria'></div></div><br><br>";		

		    $('#dat_table_step1').html(html_step1);
		    $('#dat_proceed_step1').html(html_step1_1);
		    $("#submit_cds").click(function(e){
			e.preventDefault();
		
			$("#step2").css('display','block');
			$("html,body").animate({ scrollTop: 800 }, "slow");
		    });
		    

		    $('#load_screen').modal('hide');  //display loading dialog.
		    $('#imagesearch').css('display','block');
		    var top_step1 = $('#dat_table_step1').position().top;
		    $("html,body").animate({ scrollTop: 800 }, "slow");
		    $('#example').DataTable();
		}else{

		    $('#dat_table_step1').html("<div class='alert alert-warning'><strong>Warning!</strong> Indicates a warning that might need attention.</div>");

		}
		
	    }else{

		$("#custom_alert").html("<div class='alert alert-danger' role='alert'><strong>Oooooops. </strong> Fragmenting failed.</div><br><br>");

	    }
	},
	error : function(request,error)
	{
            console.log("File couldn't be converted..");
	}
    });
}

//smiles drawing, also very similar
function getSmiles_draw() {

    $('#load_screen').modal('show');
    $("#custom_alert").html("<div class='alert alert-success' role='alert'><strong> Generating candidates.. </strong> Please wait a moment.</div><br><br><center><img src='slike/pie.svg' alt='Lscrean' style='width:100px;height:70px;'></center>");

    $.ajax({

	url : url_smile_draw,
	type : 'POST',
	data : {
            'smiles_draw' : jsmeApplet.smiles()
	},

	success : function(data) {              

	    if (data.indexOf("Fragmenting") == -1){
		$('#load_screen').modal('hide');

	    	var html_step1 =
		    "<p class='h1'>Current ligand selection</p>"
		    +"<table id='data_table_step1' class='table table-striped' cellspacing='0'>"
		    +"<input id='t_cands' type='button' class='button btn-success' value='Toggle selection' onclick='toggle_candidates()'></input>"
		    +"<thead>"
		    +"<tr>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"<th></th>"
		    +"</tr>"
		    +"</thead>"
		    +"<tbody>";
		
		html_step1 += "<tr>";
		var counter = 0;

		var candidates = data.split("\n");
		if (candidates.length > 0){
		    global_all = [];
		    all_candidates = [];


		    candidates.forEach(function(candidate){
			candidate = candidate.replace("_onlyCore.pdb","");
			var url = 'lepse_slikice/'+candidate+".png";

			if (UrlExists(url) == true){
			    console.log(candidate);
			    counter ++;
			    global_all.push(candidate);
			    all_candidates.push(candidate);
			    if (counter %9 == 0){
				var candID = "cand_"+candidate.replace(/\D/g,'');
				html_step1 += "<td><img id='"+candID+"' class='lig_image_s2' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
				html_step1 += "</tr>";
				html_step1 += "<tr>";
			    }else{
				var candID = "cand_"+candidate.replace(/\D/g,'');
				html_step1 += "<td><img id='"+candID+"' class='lig_image_s1' src='lepse_slikice/"+candidate+".png' alt='"+candidate+" ' style='width:100px;height:100px;' onclick='apc(this)'></td>";
			    }		
			}						
		    });

		    html_step1 += "</tr>";
		    html_step1 += "</tbody></table>";
		    var html_step1_1 = "<br><div class='form-group'><label class='col-md-5 control-label' for='button2id'>Add candidates and proceed..</label><div class='col-md-6'><input type='button' id='submit_cds' class='btn btn-success' value='Submit criteria'></div></div><br><br>";		

		    $('#dat_table_step1').html(html_step1);
		    $('#dat_proceed_step1').html(html_step1_1);
		    $("#submit_cds").click(function(e){
			e.preventDefault();
			//console.log("new table opened...");
			$("#step2").css('display','block');
			$("html,body").animate({ scrollTop: 800 }, "slow");
		    });
		    
		    //activate_tab('main_results','main_search')
		    $('#load_screen').modal('hide');  //display loading dialog.
		    $('#imagesearch').css('display','block');
		    var top_step1 = $('#dat_table_step1').position().top;
		    $("html,body").animate({ scrollTop: 800 }, "slow");
		    $('#example').DataTable();
		}else{
		    $('#dat_table_step1').html("<div class='alert alert-warning'><strong>Warning!</strong> Indicates a warning that might need attention.</div>");

		}
	    }else{

		$("#custom_alert").html("<div class='alert alert-danger' role='alert'><strong>Oooooops. </strong> Search failed.</div><br><br>");

	    }
	},
	error : function(request,error)
	{
            console.log("File couldn't be converted..");
	}
    });

    
}



// replace function

function replaceAll(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function construct_selection(object){

    if (object.checked == true){

	$('#core_evade').hide()
	
    }else{

	$('#core_evade').show()
    }
    
}
